import 'package:flutter/material.dart';
import 'package:test_app/dp.dart';
import 'package:test_app/pages/home_page/presentation/home_page.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DependenciesProvider(
      child: MaterialApp(
        home: HomePage(),
      ),
    );
  }
}
