import 'package:hive_flutter/hive_flutter.dart';

class AppGlobalState {
  static final AppGlobalState _singleton = AppGlobalState._internal();

  factory AppGlobalState() => _singleton;

  static const bool isProduction = bool.fromEnvironment('dart.vm.product');
  static late Box hive;
  static late String appVersion;

  AppGlobalState._internal();

  static late bool isAppleSignInAvailable;

  Future<void> appSetUp() async {
    await Hive.initFlutter();
    hive = await Hive.openBox('globalHive');
  }
}
