import 'package:flutter/material.dart';

class ToastUtils {
  /// show toast message
  static void showShortToast(BuildContext context, String message,
      [Color? color]) {
    dismissToast(context);
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        behavior: SnackBarBehavior.floating,
        backgroundColor: color ?? Colors.black.withOpacity(0.9),
        duration: const Duration(seconds: 3),
        content: Row(
          children: [
            Flexible(
              child: Text(message,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 16.0,
                          color: Colors.black)
                      .copyWith(color: Colors.white)),
            ),
          ],
        ),
      ),
    );
  }

  static void dismissToast(BuildContext context) {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
  }
}
