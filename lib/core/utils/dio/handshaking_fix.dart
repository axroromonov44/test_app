import 'dart:io';
import 'api_client.dart';
import 'package:dio/adapter.dart';

void dioClientAdapterFixHandshaking() {
  (DioClient.dio.httpClientAdapter as DefaultHttpClientAdapter)
      .onHttpClientCreate = (client) {
    client.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    return client;
  };
}
