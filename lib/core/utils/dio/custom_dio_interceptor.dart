import 'package:dio/dio.dart';
import 'package:test_app/core/utils/print_log.dart';

class CustomDioInterceptor extends Interceptor {
  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    printLog("err: ${err.requestOptions.uri}");
    printLog("type: ${err.type}");
    printLog(err.response?.statusCode.toString());
    super.onError(err, handler);
  }

  @override
  void onRequest(
      RequestOptions options, RequestInterceptorHandler handler,) async {
    if (options.data.toString() == "{}") {
      options.data = null;
    }
    super.onRequest(options, handler);
  }
}
