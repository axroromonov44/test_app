import 'package:dio/dio.dart';
import 'package:test_app/core/utils/dio/custom_dio_interceptor.dart';
import 'package:test_app/core/utils/print_log.dart';

class DioClient {
  static Dio dio = Dio()
    ..interceptors.addAll([
      CustomDioInterceptor(),
    ]);

  static void initiateDioTimeOut() {
    DioClient.dio.options.connectTimeout = 12000;
    DioClient.dio.options.receiveTimeout = 12000;
    DioClient.dio.options.sendTimeout = 12000;
  }

  static Future<Response> getRequest(String url) async {
    printLog(url);
    return await dio.get(url);
  }
}
