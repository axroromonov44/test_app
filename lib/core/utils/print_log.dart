import 'package:flutter/foundation.dart';

void printLog(dynamic child){
  const debug = true;
  if(kDebugMode && debug){
    print(child.toString());
  }
}

