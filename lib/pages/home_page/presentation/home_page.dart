import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app/core/utils/toast_utils.dart';
import 'package:test_app/pages/home_page/bloc/user_info_bloc.dart';
import 'package:test_app/pages/home_page/data/datasources/remote_user_repository.dart';
import 'package:test_app/pages/home_page/data/model/user_model.dart';
import 'package:test_app/pages/home_page/presentation/widget/list_widget.dart';
import 'package:test_app/pages/home_page/presentation/widget/shimmer_widget.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<UserModel> userModel = [];
  late UserInfoBloc _bloc;

  @override
  void initState() {
    _bloc = UserInfoBloc(RemoteUserRepository())..add(FetchUserInfoEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey,
      appBar: AppBar(
        backgroundColor: Colors.white60,
        title: const Text(
          'Users Info',
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
      ),
      body: BlocConsumer<UserInfoBloc, UserInfoState>(
        bloc: _bloc,
        listener: (context, state) {
          if (state is UserInfoLoadingErrorState) {
            ToastUtils.showShortToast(context, state.message);
          }
        },
        builder: (context, state) {
          if (state is UserInfoLoadingState) {
            return const ShimmerWidget();
          } else if (state is UserInfoLoadedState) {
            userModel = state.userModel;
          }
          return userModel.isNotEmpty
              ? RefreshIndicator(
                  onRefresh: () => refresh(),
                  child: ListWidget(userModel: userModel),
                )
              : const Center(
                  child: Text('Users info not found'),
                );
        },
      ),
    );
  }

  Future<void> refresh() async {
    _bloc.add(FetchUserInfoEvent());
    return Future.delayed(const Duration(seconds: 2));
  }
}
