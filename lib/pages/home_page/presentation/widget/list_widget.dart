import 'package:flutter/material.dart';
import 'package:test_app/pages/home_page/data/model/user_model.dart';

class ListWidget extends StatelessWidget {
  final List<UserModel> userModel;

  const ListWidget({Key? key, required this.userModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListView.separated(
          physics: const BouncingScrollPhysics(),
          shrinkWrap: true,
          itemBuilder: (context, index) {
            return ListTile(
              tileColor: Colors.white24,
              shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(12),
                      topRight: Radius.circular(12),
                      bottomRight: Radius.circular(12),
                      bottomLeft: Radius.circular(12))),
              title: Text(
                '${userModel[index].name}',
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: SelectableText.rich(
                TextSpan(
                  children: [
                    const TextSpan(
                      text: 'Email\n',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    TextSpan(text: userModel[index].email),
                    const TextSpan(
                      text: '\n\nDescription\n',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    TextSpan(text: userModel[index].gender.toString()),
                  ],
                ),
              ),
            );
          },
          separatorBuilder: (context, index) {
            return const SizedBox(
              height: 10,
            );
          },
          itemCount: userModel.length),
    );
  }
}
