import 'package:test_app/core/utils/api.dart';
import 'package:test_app/core/utils/dio/api_client.dart';
import 'package:test_app/pages/home_page/data/model/user_model.dart';

class RemoteUserRepository {
  Future<List<UserModel>> getUserInfo() async {
    final userResponse =
        await DioClient.getRequest('${API.getUserInfo}/public/v2/users');

    return (userResponse.data as List)
        .map((e) => UserModel.fromJson(e))
        .toList();
  }
}
