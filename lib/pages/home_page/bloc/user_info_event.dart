part of 'user_info_bloc.dart';

abstract class UserInfoEvent extends Equatable {
  const UserInfoEvent();
}

class FetchUserInfoEvent extends UserInfoEvent {
  @override
  List<Object?> get props => [];
}

