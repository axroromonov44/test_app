import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app/pages/home_page/data/datasources/remote_user_repository.dart';
import 'package:test_app/pages/home_page/data/model/user_model.dart';

part 'user_info_event.dart';

part 'user_info_state.dart';

class UserInfoBloc extends Bloc<UserInfoEvent, UserInfoState> {
  final RemoteUserRepository _remoteUserRepository;

  UserInfoBloc(
    this._remoteUserRepository,
  ) : super(UserInfoLoadingState()) {
    on<FetchUserInfoEvent>(getUserInfo);
  }

  void getUserInfo(
      FetchUserInfoEvent event, Emitter<UserInfoState> emit) async {
    try {
      emit(UserInfoLoadingState());
      final userInfo = await _remoteUserRepository.getUserInfo();
      emit(UserInfoLoadedState(userInfo));
    } on DioError catch (e) {
      if (e.type == DioErrorType.response) {
        if (e.response!.statusCode == 400) {
          emit(const UserInfoLoadingErrorState('Error occurred'));
        } else {
          emit(const UserInfoLoadingErrorState('Error loading user data'));
        }
      } else {
        emit(const UserInfoLoadingErrorState('No Internet Connection'));
      }
    }
  }
}
