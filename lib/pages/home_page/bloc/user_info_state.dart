part of 'user_info_bloc.dart';

abstract class UserInfoState extends Equatable {
  const UserInfoState();
}

class UserInfoLoadingState extends UserInfoState {
  @override
  List<Object> get props => [];
}

class UserInfoLoadedState extends UserInfoState {
  final List<UserModel> userModel;

  const UserInfoLoadedState(this.userModel);

  @override
  List<Object> get props => [userModel];
}

class UserInfoLoadingErrorState extends UserInfoState {
  final String message;

  const UserInfoLoadingErrorState(this.message);

  @override
  List<Object> get props => [message];
}
