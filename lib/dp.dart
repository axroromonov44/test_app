import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app/pages/home_page/bloc/user_info_bloc.dart';
import 'pages/home_page/data/datasources/remote_user_repository.dart';

class DependenciesProvider extends StatelessWidget {
  final Widget child;

  const DependenciesProvider({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (BuildContext context) =>
              UserInfoBloc(RemoteUserRepository())..add(FetchUserInfoEvent()),
        )
      ],
      child: child,
    );
  }
}
